/* eslint-env browser */
import io from 'socket.io-client';
import $ from 'jquery';

const socket = io('//localhost:3000');

// hiding label after 2.5 seconds
const delay = 2500;
setTimeout(function() { $('#label-wrapper').hide(); }, delay);
$('#main-icon').mouseout(function() { $('#label-wrapper').hide(); });
$('#main-icon').mouseover(function() { $('#label-wrapper').css('display', 'flex'); });

// Toggleling chat when clicking elements
$(document).on('click', '#main-icon', function() { toggleChat(); });
$(document).on('click', '#close-button', function() { toggleChat(); });

// Toggleling between showing and hidding the chat
let toggleChat = function(){
  if ($('#chat-component').css('display') === 'block') {
    $('#chat-icon').show();
    $('#close-icon').hide();
    $('#label').text('Click to open chat');
    $('#chat-component').hide();
  } else {
    $('#close-icon').show();
    $('#chat-icon').hide();
    $('#label').text('Click to close chat');
    $('#chat-component').show();
  }
};

// Generate html for options
let generateHtmlOptions = function(options){

  if (options) { // of receives arguments, build with those
    options.forEach(function (item) {
      $('#options-wrapper').append($(`<button class="option">${item}</button>`));
    });
  } else { // if no arguments, generate default options
    const startOptions = ['Activities nearby', 'Booking a room'];
    startOptions.forEach(function (item) {
      $('#options-wrapper').append($(`<button class="option">${item}</button>`));
    });
  }
};


// Detect if user is selecting a room type
let detectRoomSelection = function (choice) {

  if (
    choice === 'Single bed' ||
    choice === 'Queen size bed' ||
    choice === 'Twin' ||
    choice === 'Suite' ||
    choice === 'Penthouse'
  ) {
    return true;
  }
};


// generates html for the users message
let generateUserMessage = function (text) {

  if (text === '') { //checking for empty input field
    return false;
  } else {

    if (detectRoomSelection(text)) { // Checking for room type

      socket.emit('room type', text);

    } else if (parseInt(text, 10)) { // Checking if answer is number and emit different event

      socket.emit('days of stay', parseInt(text, 10));

    } else {

      socket.emit('chat message', text);

    }

    $('#messages').append($(`<li class="me"><div class="me-message">${text}</div></li>`));
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').val('');

  }

  return false;
};


// detects click on the text options that the user can say to the bot
$(document).on('click', '.option', function(e) {
  var optionSelected = e.target.innerHTML;
  generateUserMessage(optionSelected);
});
// handling input field submission
$('#chat-form').submit(function() {
  generateUserMessage($('#chat-input').val());
  return false;
});


// Process response and generates html bot messages
socket.on('chat message', function(msg){

  // removes existing options
  $('#options-wrapper').find('.option').remove();

  if (typeof msg === 'string') {

    $('#messages').append($(`<li class="bot"><div class="bot-message">${msg}</div></li>`));
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').attr('placeholder', 'Type something');

    generateHtmlOptions();

  } else if (msg.question === 'How many days are you staying?') {

    $('#messages').append(`<li class="bot"><div class="bot-message">${msg.reply}<br />${msg.question}</div></li>`);
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').attr('placeholder', 'Type number of days here');
    $('#chat-input').focus();

  } else if (msg.link) {

    $('#messages').append(`
      <li class="bot">
        <div class="bot-message">
          <a href="${msg.link}" target="_blank">${msg.reply}</a>
          <br />${msg.question}
        </div>
      </li>`);
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').attr('placeholder', 'Type something');

    generateHtmlOptions();

  } else if (msg.options) {

    // msg.options.forEach(function (item) {
    //   $('#options-wrapper').append($(`<button class="option">${item}</button>`));
    // });

    $('#messages').append($(`<li class="bot"><div class="bot-message">${msg.question}</div></li>`));
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').attr('placeholder', 'Type something');

    generateHtmlOptions(msg.options);

  } else {

    $('#messages').append(`<li class="bot"><div class="bot-message">${msg.reply}<br />${msg.question}</div></li>`);
    $('#messages').scrollTop($('#messages').height());
    $('#chat-input').attr('placeholder', 'Type something');

    generateHtmlOptions();

  }

});
