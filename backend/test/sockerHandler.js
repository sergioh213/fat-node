var assert = require('assert');
var sinon = require('sinon');
var SocketHandler = require('./../src/socketHandler');

describe('SocketHandler', function() {
  describe('#onConnect', function() {
    it('should log "a user connected"', function() {
      const c = { log: sinon.spy() };
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onConnect({c})
      assert.equal(c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(c.log.getCall(0).args[0], 'a user connected', 'Inappropriate message logged');
    });
  });

  describe('#onDisconnect', function() {
    it('should log "a user disconnected"', function() {
      const c = { log: sinon.spy() };
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onDisconnect({c})
      assert.equal(c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(c.log.getCall(0).args[0], 'a user disconnected', 'Inappropriate message logged');
    });
  });

  describe('#onMessage', function() {
    it('should respond with "Sorry, I don\'t know what you mean. Tell me again, how can I help you?" by default', function() {
      const input = {
        msg: '',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: ', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1], 'Sorry, I don\'t know what you mean. Tell me again, how can I help you?', 'Wrong reply emitted on socket');
    });

    it('should respond with "Hey Alice. How can I help?"', function() {
      const input = {
        msg: 'Hel l o',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: Hel l o', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].reply, 'Hey Alice', 'Wrong reply emitted on socket');
    });

    it('should respond with "What kind of room would you like?"', function() {
      const input = {
        msg: 'bookingaroom',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: bookingaroom', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].question, 'What kind of room would you like?', 'Wrong question emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].options[0], 'Single bed', 'Wrong options emitted on socket');
    });
    it('should respond with "These are the nearby activities. What would you like to do?"', function() {
      const input = {
        msg: 'activitiesnearby',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: activitiesnearby', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].question, 'These are the nearby activities. What would you like to do?', 'Wrong question emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].options[0], 'Landmarks', 'Wrong options emitted on socket');
    });
    it('should respond with "These are the nearby activities. What would you like to do?"', function() {
      const input = {
        msg: 'bars',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: bars', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].question, 'Anything else?', 'Wrong question emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].reply, 'Here are the bars in the area.', 'Wrong reply emitted on socket');
    });
    it('should respond with "Anything else?"', function() {
      const input = {
        msg: 'landmarks',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: landmarks', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].question, 'Anything else?', 'Wrong question emitted on socket');
    });
    it('should respond with "Anything else?"', function() {
      const input = {
        msg: 'museums',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onMessage(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: museums', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].question, 'Anything else?', 'Wrong question emitted on socket');
    });
  });
  describe('#onNumOfDays', function() {
    it('should respond with "Sorry, I don\'t know what you mean. Tell me again, how can I help you?"', function() {
      const input = {
        numOfDays: 3,
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onNumOfDays(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: 3', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1], 'Sorry, I don\'t know what you mean. Tell me again, how can I help you?', 'Wrong reply emitted on socket');
    });
    it('should respond with a valid string', function() {
      const input = {
        numOfDays: 10,
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onNumOfDays(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: 10', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(typeof input.socket.emit.getCall(0).args[1], 'string', 'Reply is not a string');
      assert.equal(input.socket.emit.getCall(0).args[1].length > 0, true, 'Reply is an empty string');
    });
  })
  describe('#onRoomChoice', function() {
    it('should respond with "The Single bed room has a price per night of 80€"', function() {
      const input = {
        room: 'Single bed',
        socket: {
          emit: sinon.spy()
        },
        c: { log: sinon.spy() }
      }
      const socketHandler = new SocketHandler.SocketHandler();
      socketHandler.onRoomChoice(input)
      assert.equal(input.c.log.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.c.log.getCall(0).args[0], 'message: Single bed', 'Inappropriate message logged');
      assert.equal(input.socket.emit.callCount, 1 , "console.log has no been called exactly one times");
      assert.equal(input.socket.emit.getCall(0).args[0], 'chat message', 'Wrong event emitted on socket');
      assert.equal(input.socket.emit.getCall(0).args[1].reply, 'The Single bed room has a price per night of 80€', 'Wrong reply emitted on socket');
    });
  });
});
