/**
  * The SocketHandler class handles sockets
  */
class SocketHandler {

  /**
    * Creates a new SocketHandler
    */
  constructor() {
    this.prevQuestion = '',
    this.activities = {
      question: 'These are the nearby activities. What would you like to do?',
      options: ['Landmarks', 'Bars', 'Museums']
    },
    this.bars = {
      reply: 'Here are the bars in the area.',
      link: `https://www.google.com/maps/search/bars/@52.5180117,13.3747347,14z/
        data=!3m1!4b1!4m8!2m7!3m6!1sbars!2sLobby+Lounge+%26+Bar+Hotel+Adlon,+Unter+den+Linden+77,
        +10117+Berlin!3s0x47a851c68786c4d5:0x4785ddbac9de7622!4m2!1d13.380126!2d52.5159911`,
      question: 'Anything else?'
    },
    this.booking = {
      question: 'What kind of room would you like?',
      options: ['Single bed', 'Queen size bed', 'Twin', 'Suite', 'Penthouse']
    },
    this.stayDuration = {
      reply: '',
      question: 'How many days are you staying?'
    },
    this.price = {
      reply: '',
      question: 'Anything else?'
    },
    this.landmarks = {
      reply: 'Here are some of Berlin\'s Landmarks',
      link: 'https://www.google.com/maps/search/berlin+landmarks/',
      question: 'Anything else?'
    },
    this.museums = {
      reply: 'Here are some of Berlin\'s Museums',
      link: 'https://www.google.com/maps/search/berlin+museums/',
      question: 'Anything else?'
    },
    this.salutation = {
      reply: 'Hey Alice',
      question: 'How can I help?'
    },
    this.roomSelected = null,
    this.numOfDays = 0,
    this.totalPrice = 0,
    this.roomPrices = {
      'Single bed': 80,
      'Queen size bed': 120,
      'Twin': 200,
      'Suite': 1500,
      'Penthouse': 8000
    };
  }

  /**
    * Logs that a user connected
  */
  onConnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user connected');
  }

  /**
    * Logs that a user disconnected
  */
  onDisconnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user disconnected');
  }

  /**
    * Saves the selected room type
  */
  onRoomChoice({room, socket, c = console} = {}){
    c.log('message: ' + room);

    this.roomSelected = room;
    this.stayDuration.reply = `The ${room} room has a price per night of ${this.roomPrices[room]}€`;

    socket.emit('chat message', this.stayDuration);
  }

  /**
    * Saves the number of days of the stay
  */
  onNumOfDays({numOfDays, socket, c = console} = {}){
    c.log('message: ' + numOfDays);

    this.numOfDays = numOfDays;
    this.totalPrice = numOfDays * this.roomPrices[this.roomSelected];
    this.price.reply = `Your total is ${this.totalPrice}€`;

    var reply = '';
    this.roomSelected ? reply = this.price.reply :
      reply = 'Sorry, I don\'t know what you mean. Tell me again, how can I help you?';

    socket.emit('chat message', reply);
    this.roomSelected = null;
  }

  /**
    * Responds to a message
  */
  onMessage(/* istanbul ignore next */{msg, socket, c = console} = {}){
    c.log('message: ' + msg);

    let reply = 'Sorry, I don\'t know what you mean. Tell me again, how can I help you?';

    switch (msg.replace(/\s/gi,'').toLowerCase()) {
      case 'hello':
      case 'hi':
      case 'hithere':
      case 'hellothere':
        reply = this.salutation;
        break;
      case 'activitiesnearby':
        reply = this.activities;
        break;
      case 'bars':
        reply = this.bars;
        break;
      case 'bookingaroom':
        reply = this.booking;
        break;
      case 'landmarks':
        reply = this.landmarks;
        break;
      case 'museums':
        reply = this.museums;
        break;
    }

    socket.emit('chat message', reply);
  }
}

exports.SocketHandler = SocketHandler;
