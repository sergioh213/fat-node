# Flexperto - Fullstack NodeJS Test

## Intro

This is a chatbot made by Sergio Herrero.

### User Journey

```
Alice approaches the reception.

The receptionist offers to help with booking a room or finding nearby activities

Alice choses nearby activities.

The receptionist asks Alice to choose between nearby bars, museums or landmarks to visit.

In any of the three choices, Alice will be redirected to a map with various options.

The receptionist offers help again.

Alice chooses to book a room.

The receptionist asks what kind of room from a list of available types.

Alice chooses a room.

The receptionist says the price per night for that type of room and asks how many night she wants to stay.

Alice says the amount of days.

The receptionist tells Alice the total amount to be paid. And offers to help with anything else once again.

```
At the end of the line the bot will always ask: "Anything else?", and give the starting options again

### Documentation

Note: In order to run this project, I'm assuming that you have docker installed.

**Follow this steps to run the project:**

  + Clone the [repository](https://bitbucket.org/sergioh213/fat-node/src/master/).
  + On your terminal, ```cd``` into the project's directory.
  + Run ```docker-compose up``` on your terminal.
  + Navigate to http://localhost:4000/ on your web browser

For testing:

  + Make sure that your ```docker-compose up``` is running. If not, run the command ```docker-compose up``` on your terminal.
  + Open a new tab on your terminal.
  + Replace *{container_id}* with your docker container ID and run ```docker exec -it {container_id} npm run verify``` on your terminal.
  + If you don't know your docker container ID. Run ```docker ps``` on your terminal. This will list your containers. You should see two rows, one for the *frontend* container and one for the *backend* container. Copy the ID of the container you wanted to test, they should be below the column ```CONTAINER ID```.
  + Run ```docker exec -it {container_id} npm run verify``` on your terminal replacing *{container_id}* with the ID you copied.
